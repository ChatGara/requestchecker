# RequestChecker

## Description

Petite application de démonstration destiné a :
- Logger un warning si on a pas de header X-requester
- Transiter le X-requester à l'appelant suivant

Cette application à plus vocation a être testé via des tests unitaires

A terme le but étant de pouvoir identifier les applications qui n'envoit pas de header X-requester
(et de les blamer :D !)

Pas de X-Requester Header : 

![img.png](img/No X-Requester Header.png)

Avec X-Requester Header : 

![img.png](img/With X-Requester Header.png)

## Sources

* https://www.baeldung.com/spring-extract-custom-header-request
* https://www.baeldung.com/java-feign-request-headers


