package com.checker.externalapi.myincredibleapi.controller;

import com.checker.externalapi.myincredibleapi.model.AwesomeData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "MyIncredible-api", url = "http://localhost:8888", path = "/")
public interface MyIncredibleApi {

    @RequestMapping(method = RequestMethod.GET, value="/some-awesome-data")
    List<AwesomeData> getSomeAwesomeData();
}
