package com.checker.externalapi.myincredibleapi.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AwesomeData {
    private String code;
}
