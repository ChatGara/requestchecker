package com.checker.configuration;

import com.checker.interceptor.XRequesterHandlerInterceptor;
import com.checker.interceptor.model.XRequesterHolder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class HeaderInterceptorConfig implements WebMvcConfigurer {

    public static String X_REQUESTER_HEADER = "X-requester";

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(headerHandlerInterceptor());
    }

    @Bean
    public XRequesterHandlerInterceptor headerHandlerInterceptor() {
        return new XRequesterHandlerInterceptor(operatorHolder());
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public XRequesterHolder operatorHolder() {
        return new XRequesterHolder();
    }

}
