package com.checker.controller;

import com.checker.externalapi.myincredibleapi.controller.MyIncredibleApi;
import com.checker.externalapi.myincredibleapi.model.AwesomeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DummyController {

    @Autowired
    private MyIncredibleApi myIncredibleApi;

    @GetMapping("/dummy")
    public ResponseEntity<String> getDummy() {
        return new ResponseEntity<String>("I'm so dummy", HttpStatus.OK);
    }

    @GetMapping("/some-data")
    public ResponseEntity<List<AwesomeData>> getCrewActivityTypes() {
        return new ResponseEntity<>(myIncredibleApi.getSomeAwesomeData(), HttpStatus.OK);
    }

}
