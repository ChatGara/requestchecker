package com.checker.interceptor.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class XRequesterHolder {
    private String xRequester;
}

