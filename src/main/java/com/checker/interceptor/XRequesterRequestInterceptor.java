package com.checker.interceptor;

import com.checker.interceptor.model.XRequesterHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

import static com.checker.configuration.HeaderInterceptorConfig.X_REQUESTER_HEADER;

import java.util.Optional;

@Component
public class XRequesterRequestInterceptor implements RequestInterceptor {

    private static final String X_REQUESTER_APPLICATION_NAME = "RequestChecker";

    private final XRequesterHolder xRequesterHolder;

    public XRequesterRequestInterceptor(XRequesterHolder xRequesterHolder) {
        this.xRequesterHolder = xRequesterHolder;
    }

    @Override
    public void apply(RequestTemplate template) {
        Optional<String> xRequester = Optional.ofNullable(xRequesterHolder.getXRequester());
        xRequester.ifPresent(xReq -> template.header(X_REQUESTER_HEADER, xReq));
    }
}
