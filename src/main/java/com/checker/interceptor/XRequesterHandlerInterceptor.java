package com.checker.interceptor;

import com.checker.interceptor.model.XRequesterHolder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import static com.checker.configuration.HeaderInterceptorConfig.X_REQUESTER_HEADER;

import java.util.Optional;

@Slf4j
@Component
public class XRequesterHandlerInterceptor implements HandlerInterceptor {

    private final XRequesterHolder xRequesterHolder;

    public XRequesterHandlerInterceptor(XRequesterHolder xRequesterHolder) {
        this.xRequesterHolder = xRequesterHolder;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Optional<String> xRequesterHeader = Optional.ofNullable(request.getHeader(X_REQUESTER_HEADER));
        if (xRequesterHeader.isPresent()) {
            xRequesterHolder.setXRequester(xRequesterHeader.get());
            log.warn("X-requester header found : [" + xRequesterHeader.get() + "] for " + request.getRequestURL());
        }
        else {
            log.warn("No X-requester header found for " + request.getRequestURL());
        }
        return true;
    }
}
