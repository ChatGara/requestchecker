package com.checker.controller;


import com.checker.interceptor.XRequesterHandlerInterceptor;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.test.appender.ListAppender;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DummyControllerTest {

    private WireMockServer wireMockServer;

    public static String X_REQUESTER_HEADER = "X-requester";

    @Autowired
    private MockMvc mvc;

    @Nested
    class TestXRequesterHeaderLog {
        private ListAppender appender;

        @BeforeEach
        public void setUp() {
            LoggerContext loggerContext = LoggerContext.getContext(false);
            Logger logger = (Logger) loggerContext.getLogger(XRequesterHandlerInterceptor.class);
            this.appender = new ListAppender(RandomStringUtils.random(50, true, false));
            this.appender.start();
            loggerContext.getConfiguration().addLoggerAppender(logger, this.appender);
        }

        @AfterEach()
        public void tearDown() {
            this.appender.clear();
        }

        @Test
        public void whenCallDummyWithoutXRequesterHeader_thenItShouldLogAWarningXRequesterNotFound() throws Exception {
            // GIVEN
            String expectedLog = "No X-requester header found for http://localhost/dummy";
            // WHEN
            mvc.perform(MockMvcRequestBuilders.get("/dummy")).andExpect(status().isOk());

            // THEN
            Assertions.assertThat(this.appender.getEvents().stream().map(event1 -> event1.getMessage().toString()).toList().size()).isEqualTo(1);
            Assertions.assertThat(this.appender.getEvents().stream().map(event -> event.getMessage().toString()).toList().get(0)).isEqualTo(expectedLog);
        }

        @Test
        public void whenCallDummyWithoutXRequesterHeader_thenItShouldLogAWarningXRequesterFound() throws Exception {
            // GIVEN
            String expectedLog = "X-requester header found : [a incredible request header] for http://localhost/dummy";
            // WHEN
            mvc.perform(
                    MockMvcRequestBuilders.get("/dummy")
                            .header(X_REQUESTER_HEADER, "a incredible request header")
            ).andExpect(status().isOk());

            // THEN
            List<String> loggedStrings =
                    this.appender.getEvents().stream().map(event -> event.getMessage().toString()).toList();

            Assertions.assertThat(loggedStrings.size()).isEqualTo(1);
            Assertions.assertThat(loggedStrings.get(0)).isEqualTo(expectedLog);

        }
    }

    @Nested
    class TestTransmittingXRequesterHeader {
        @BeforeEach
        public void setUp() {
            wireMockServer = new WireMockServer(wireMockConfig().port(8888));
            wireMockServer.start();
        }

        @AfterEach()
        public void tearDown() {
            wireMockServer.stop();
        }

        @Test
        public void whenCallCrewActivityTypeWithXRequesterHeader_thenItShouldTransmitHeaderToNextService() throws Exception {
            // GIVEN
            wireMockServer.stubFor(get("/some-awesome-data")
                    .willReturn(ok()));

            // WHEN
            mvc.perform(
                    MockMvcRequestBuilders.get("/some-data")
                            .header(X_REQUESTER_HEADER, "a incredible request header")
            ).andExpect(status().isOk());

            // THEN
            wireMockServer.verify(
                    getRequestedFor(urlEqualTo("/some-awesome-data"))
                            .withHeader(X_REQUESTER_HEADER, equalTo("a incredible request header")));
        }

        @Test
        public void whenCallCrewActivityTypeWithNoXRequesterHeader_thenItShouldTransmitNothingToNextService() throws Exception {
            // GIVEN
            wireMockServer.stubFor(get("/some-awesome-data")
                    .willReturn(ok()));

            // WHEN
            mvc.perform(
                    MockMvcRequestBuilders.get("/some-data")
            ).andExpect(status().isOk());

            // THEN
            wireMockServer.verify(
                    getRequestedFor(urlEqualTo("/some-awesome-data"))
                            .withoutHeader(X_REQUESTER_HEADER));
        }
    }



}
